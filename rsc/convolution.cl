__constant sampler_t sampler =
    CLK_NORMALIZED_COORDS_FALSE |
    CLK_FILTER_LINEAR |
    CLK_ADDRESS_CLAMP;

__kernel void convolution(__read_only image2d_t input, __write_only image2d_t output,
    int width, int height, __constant float* filter, int filterWidth, float factor, sampler_t sampler)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

    int filterOffset = filterWidth / 2;

    int4 pixel = (int4){0, 0, 0, 0};
    int2 coords;
    int filterIdx = 0;
    for (int j = -filterOffset; j <= filterOffset; ++j)
    {
        coords.y = y + j;
        for (int i = -filterOffset; i <= filterOffset; ++i)
        {
            coords.x = x + i;

            float f = filter[filterIdx++];
            pixel.x += read_imageui(input, sampler, coords).x * f;
            pixel.y += read_imageui(input, sampler, coords).y * f;
            pixel.z += read_imageui(input, sampler, coords).z * f;
        }
    }
    pixel.x *= factor;
    pixel.y *= factor;
    pixel.z *= factor;
    pixel.w = read_imageui(input, sampler, (int2)(x, y)).a;
    write_imageui(output, (int2)(x, y), convert_uint4_sat(pixel));
}
