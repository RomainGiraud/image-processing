__constant sampler_t sampler =
    CLK_NORMALIZED_COORDS_FALSE |
    CLK_FILTER_LINEAR |
    CLK_ADDRESS_CLAMP;

__kernel void rotate(__read_only image2d_t input, __write_only image2d_t output,
    int width, int height, float theta)
{
    int x = get_global_id(0);
    int y = get_global_id(1);

    float x0 = width / 2.f;
    float y0 = height / 2.f;

    int xprime = x - x0;
    int yprime = y - y0;

    float sinT = sin(theta);
    float cosT = cos(theta);

    float2 readCoord;
    readCoord.x = xprime * cosT - yprime * sinT + x0;
    readCoord.y = xprime * sinT + yprime * cosT + y0;

    uint4 value = read_imageui(input, sampler, readCoord);
    //uint4 value = (uint4){255, 0, 0, 255};
    write_imageui(output, (int2)(x, y), value);
}