#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <string>

#include <Eigen/Core>

class Image
{
public:
    virtual ~Image() {}
    virtual void reset(int width, int height) = 0;

    virtual void load(std::string filename) = 0;
    virtual void save(std::string filename) = 0;

    virtual unsigned int    width()  const = 0;
    virtual unsigned int    height() const = 0;
    virtual Eigen::Vector2f size()   const = 0;

	virtual unsigned char* data() = 0;
	virtual const unsigned char* data() const = 0;
    virtual void setData(unsigned char *data, const Eigen::Vector2f &size) = 0;

    virtual Eigen::Vector4f operator()(unsigned int x, unsigned int y) const = 0;
    Eigen::Vector4f operator()(const Eigen::Vector2f &position) const;
};

#endif /*__IMAGE_H__*/
