#include "Convolution.h"

#include <stdexcept>

using namespace std;
using namespace Eigen;

Convolution::Convolution(const Eigen::MatrixXf &filter, float factor)
    : _filter(filter), _factor(factor)
{
    if (_filter.cols() != _filter.rows())
        throw runtime_error("Filter matrix is not a square");

    if (_filter.cols() == 1 || _filter.cols() % 2 != 1)
        throw runtime_error("Filter size is not odd");
}

Convolution::~Convolution()
{
}