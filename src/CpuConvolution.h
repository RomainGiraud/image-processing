#ifndef __CPUCONVOLUTION_H__
#define __CPUCONVOLUTION_H__

#include "Convolution.h"

class CpuConvolution : public Convolution
{
public:
    CpuConvolution(const Eigen::MatrixXf &filter, float factor);
    ~CpuConvolution();
    void process(const Image &input, Image &output) const;
};

#endif /*__CPUCONVOLUTION_H__*/
