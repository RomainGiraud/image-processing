#ifndef __IMAGEPNG_H__
#define __IMAGEPNG_H__

#include "Image.h"

class ImagePNG : public Image
{
public:
    ImagePNG();
    virtual ~ImagePNG();
    virtual void reset(int width, int height);

    virtual void load(std::string filename);
    virtual void save(std::string filename);

    virtual unsigned int    width()  const;
    virtual unsigned int    height() const;
    virtual Eigen::Vector2f size()   const;

    virtual unsigned char* data();
    virtual const unsigned char* data() const;
    virtual void setData(unsigned char *data, const Eigen::Vector2f &size);

    virtual Eigen::Vector4f operator()(unsigned int x, unsigned int y) const;

private:
	struct ImagePNGPrivate;
	ImagePNGPrivate *_private;

    void initialize();
};

#endif /*__IMAGEPNG_H__*/
