#include "Image.h"

Eigen::Vector4f Image::operator()(const Eigen::Vector2f &position) const
{
    return (*this)(position.x(), position.y());
}
