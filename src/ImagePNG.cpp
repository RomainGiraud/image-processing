#include "ImagePNG.h"

#include <iostream>
#include <exception>

#include <string.h>

#include <png.h>

using namespace std;

struct ImagePNG::ImagePNGPrivate
{
	unsigned int width;
	unsigned int height;
	png_byte colorType;
	png_byte bitDepth;
	png_bytepp rowPointers; // internal
	png_byte* data;

	ImagePNGPrivate()
	{
		width = 0;
		height = 0;
		colorType = 0;
		bitDepth = 0;
		rowPointers = 0;
		data = 0;
	}

	~ImagePNGPrivate()
	{
		free(rowPointers);
	}
};

ImagePNG::ImagePNG()
	: _private(new ImagePNGPrivate)
{
}

ImagePNG::~ImagePNG()
{
	delete _private;
}

void ImagePNG::reset(int width, int height)
{
	_private->width = width;
	_private->height = height;
	_private->colorType = PNG_COLOR_TYPE_RGBA;
	_private->bitDepth = 8;

	initialize();
}

void ImagePNG::load(std::string filename)
{
	unsigned char header[8];    // 8 is the maximum size that can be checked

	/* open file and test for it being a png */
	FILE *fp = fopen(filename.c_str(), "rb");
	if (!fp)
	{
		throw runtime_error("Cannot open file " + filename);
	}

	if (fread(header, 1, 8, fp) != 8)
	{
		throw runtime_error("Cannot read header of " + filename);
	}

	if (png_sig_cmp((unsigned char *)header, 0, 8))
	{
		throw runtime_error("File " + filename + " is not a PNG file");
	}


	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		throw runtime_error("png_create_read_struct failed");
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		throw runtime_error("png_create_info_struct failed");
	}

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_init_io");
	}

	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);

	png_read_info(png_ptr, info_ptr);
	_private->width = png_get_image_width(png_ptr, info_ptr);
	_private->height = png_get_image_height(png_ptr, info_ptr);
	_private->colorType = png_get_color_type(png_ptr, info_ptr);
	_private->bitDepth = png_get_bit_depth(png_ptr, info_ptr);

	if (_private->colorType == PNG_COLOR_TYPE_RGB || _private->colorType == PNG_COLOR_TYPE_GRAY)
        png_set_add_alpha(png_ptr, 255, PNG_FILLER_AFTER);

    if (_private->colorType == PNG_COLOR_TYPE_PALETTE)
  		png_set_palette_to_rgb(png_ptr);
	if (_private->colorType == PNG_COLOR_TYPE_GRAY && _private->bitDepth < 8)
  		png_set_expand_gray_1_2_4_to_8(png_ptr);
  	if (_private->colorType == PNG_COLOR_TYPE_GRAY)
  		png_set_gray_to_rgb(png_ptr);

	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
  		png_set_tRNS_to_alpha(png_ptr);

	if (_private->bitDepth == 16)
  		png_set_strip_16(png_ptr);
	else if (_private->bitDepth < 8)
  		png_set_packing(png_ptr);

	int number_of_passes = png_set_interlace_handling(png_ptr);
	png_read_update_info(png_ptr, info_ptr);
	_private->width = png_get_image_width(png_ptr, info_ptr);
	_private->height = png_get_image_height(png_ptr, info_ptr);
	_private->colorType = png_get_color_type(png_ptr, info_ptr);
	_private->bitDepth = png_get_bit_depth(png_ptr, info_ptr);

	initialize();

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_read_image");
	}

	png_read_image(png_ptr, _private->rowPointers);

	fclose(fp);

	cout << "Image loaded: " << _private->width << ", " << _private->height
		 << " | " << (int)_private->colorType << " | " << (int)_private->bitDepth << endl;
}

void ImagePNG::initialize()
{
	/*
	switch (_private->colorType)
	{
	case PNG_COLOR_TYPE_RGB:
		cout << "RGB" << endl;
		break;
	case PNG_COLOR_TYPE_RGB_ALPHA:
		cout << "RGBA" << endl;
		break;
	case PNG_COLOR_TYPE_PALETTE:
		cout << "Palette" << endl;
		break;
	case PNG_COLOR_TYPE_GRAY:
		cout << "Gray" << endl;
		break;
	case PNG_COLOR_TYPE_GRAY_ALPHA:
		cout << "Gray" << endl;
		break;
	}
	*/

	if (_private->colorType != PNG_COLOR_TYPE_RGBA)
		throw runtime_error("Invalid color type");

	if (_private->bitDepth != 8)
		throw runtime_error("Invalid bit depth");

	//_colorType = 4 pixel
	//_bitDepth = 1 byte

	if (_private->rowPointers != 0)
	{
		free(_private->rowPointers);
		_private->rowPointers = 0;
		free(_private->data);
		_private->data = 0;
	}

	_private->data = (png_byte*) malloc(_private->height * _private->width * sizeof(unsigned char) * 4);
	_private->rowPointers = (png_bytepp) malloc(sizeof(png_bytep) * _private->height);
	for (int y = 0; y < _private->height; ++y)
	{
		_private->rowPointers[y] = (png_bytep) _private->data + y * _private->width * sizeof(unsigned char) * 4;
	}
}

void ImagePNG::save(std::string filename)
{
	/* create file */
	FILE *fp = fopen(filename.c_str(), "wb");
	if (!fp)
	{
		throw runtime_error("Cannot open file " + filename);
	}

	/* initialize stuff */
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		throw runtime_error("png_create_write_struct failed");
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		throw runtime_error("png_create_info_struct failed");
	}

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_init_io");
	}

	png_init_io(png_ptr, fp);


	/* write header */
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_write_info");
	}

	png_set_IHDR(png_ptr, info_ptr, _private->width, _private->height,
		_private->bitDepth, _private->colorType, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	png_write_info(png_ptr, info_ptr);


	/* write bytes */
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_write_image");
	}

	png_write_image(png_ptr, _private->rowPointers);


	/* end write */
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		throw runtime_error("Error during png_write_end");
	}

	png_write_end(png_ptr, NULL);
}

unsigned char* ImagePNG::data()
{
	return reinterpret_cast<unsigned char *>(_private->data);
}

const unsigned char* ImagePNG::data() const
{
	return reinterpret_cast<unsigned char *>(_private->data);
}

void ImagePNG::setData(unsigned char *data, const Eigen::Vector2f &size)
{
	_private->width  = size.x();
	_private->height = size.y();
	_private->colorType = PNG_COLOR_TYPE_RGBA;
	_private->bitDepth  = 8;
	initialize();

	memcpy(_private->data, data, _private->height * _private->width * 4 * sizeof(unsigned char));
}

unsigned int ImagePNG::width() const
{
    return _private->width;
}

unsigned int ImagePNG::height() const
{
    return _private->height;
}

Eigen::Vector2f ImagePNG::size() const
{
    return Eigen::Vector2f(_private->width, _private->height);
}

Eigen::Vector4f ImagePNG::operator()(unsigned int x, unsigned int y) const
{
	png_byte* ptr = &_private->data[y * _private->width * 4 + x * 4];
    return Eigen::Vector4f(ptr[0], ptr[1], ptr[2], ptr[3]);
}
