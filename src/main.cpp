#include <iostream>

#include <Eigen/Core>

#include "ImagePNG.h"
#include "Convolution.h"
#include "CpuConvolution.h"
#include "GpuConvolution.h"

using namespace std;
using namespace Eigen;

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        cerr << "Usage: " << argv[0] << " <file_in> <file_out>" << endl;
        return 1;
    }


    //float factor = 1 / 9.f;
    //Matrix3f m;
    //m << 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f;
    //m << 0.0f, 9.0f, 0.0f, 9.0f, -36.0f, 9.0f, 0.0f, 9.0f, 0.0f;
    //m << 2.0f, 22.0f, 1.0f, 22.0f, 1.0f, -22.0f, 1.0f, -22.0f, -2.0f;
    //m << 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f;
    //m << -5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 5.0f;
    //m << -1.0f, -2.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 2.0f, 1.0f;

    //typedef Matrix<float, 5, 5> Matrix5f;
    //Matrix5f m;
    //m << 1, 4, 6, 4, 1,
    //     4, 16, 24, 16, 4,
    //     6, 24, -476, 24, 6,
    //     4, 16, 24, 16, 4,
    //     1, 4, 6, 4, 1;
    //float factor = 1 / 256.f;

    typedef Matrix<float, 3, 3> Matrix3f;
    Matrix3f m;
    m << 1, 2, 1,
         0, 0, 0,
         -1, -2, -1;
    //m << 0, -1, 0,
    //     -1, 5, -1,
    //     0, -1, 0;

    float factor = 1;
    GpuConvolution conv(m, factor);

    ImagePNG img;
    img.load(argv[1]);

    ImagePNG out;
    conv.process(img, out);
    out.save(argv[2]);

	return 0;
}
