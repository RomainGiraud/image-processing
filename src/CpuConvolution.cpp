#include "CpuConvolution.h"

using namespace std;
using namespace Eigen;

CpuConvolution::CpuConvolution(const Eigen::MatrixXf &filter, float factor)
    : Convolution(filter, factor)
{
}

CpuConvolution::~CpuConvolution()
{
}

void CpuConvolution::process(const Image &input, Image &output) const
{
    output.reset(input.width(), input.height());
    unsigned char *data = output.data();

    int offsetX = _filter.cols() / 2;
    int offsetY = _filter.rows() / 2;

    int xx = 0, yy = 0;
    for (unsigned int y = 0; y < input.height(); ++y)
    {
        for (unsigned int x = 0; x < input.width(); ++x)
        {
            Eigen::Vector3f v(0, 0, 0);
            for (int j = -offsetY; j <= offsetY; ++j)
            {
                yy = y + j;
                if (yy < 0 || yy >= input.height()) yy = y;

                for (int i = -offsetX; i <= offsetX; ++i)
                {
                    xx = x + i;
                    if (xx < 0 || xx >= input.width()) xx = x;

                    v += input(xx, yy).head<3>() * _filter(j + offsetY, i + offsetX);
                }
            }
            v *= _factor;

            unsigned index = input.width() * 4 * y + x * 4;
            data[index + 0] = max(min(v.x(), 255.0f), 0.0f);
            data[index + 1] = max(min(v.y(), 255.0f), 0.0f);
            data[index + 2] = max(min(v.z(), 255.0f), 0.0f);
            data[index + 3] = input(x, y).w();
        }
    }
}
