#ifndef __CONVOLUTION_H__
#define __CONVOLUTION_H__

#include <Eigen/Core>

#include "Image.h"

class Convolution
{
public:
    Convolution(const Eigen::MatrixXf &filter, float factor);
    virtual ~Convolution();
    virtual void process(const Image &input, Image &output) const = 0;

protected:
    Eigen::MatrixXf _filter;
    float _factor;
};

#endif /*__CONVOLUTION_H__*/
