#include "GpuConvolution.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <CL/cl.h>

using namespace std;
using namespace Eigen;

const char *getClErrorString(cl_int error);
void clCheck(cl_int status);

struct GpuConvolution::GpuConvolutionPrivate
{
    cl_device_id device;
    cl_context context;
    cl_program program;
    cl_kernel kernel;
    cl_command_queue queue;

    GpuConvolutionPrivate() {}
    ~GpuConvolutionPrivate();

    void initializeOpenCL();
    void buildProgram(const std::string &filename);
};

GpuConvolution::GpuConvolutionPrivate::~GpuConvolutionPrivate()
{
    clReleaseCommandQueue(queue);
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseContext(context);
}

void GpuConvolution::GpuConvolutionPrivate::buildProgram(const std::string &filename)
{
    ifstream is;
    is.open(filename.c_str(), ios::binary);

    // get length of file:
    is.seekg(0, ios::end);
    size_t length = is.tellg();
    is.seekg(0, ios::beg);

    // allocate memory:
    char* buffer = new char[length];

    // read data as a block:
    is.read(buffer, length);
    is.close();


    /* Create program from file */
    int err;
    program = clCreateProgramWithSource(context, 1,
        (const char**)&buffer, &length, &err);
    if (err < 0)
    {
        throw runtime_error("Couldn't create the program");
    }
    delete buffer;


    /* Build program */
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err < 0)
    {
        /* Find size of log and print to std output */
        size_t log_size;
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

        char* program_log = new char[log_size + 1];
        program_log[log_size] = '\0';
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
        string log(program_log, log_size);
        delete program_log;

        throw runtime_error(log);
    }
}

void GpuConvolution::GpuConvolutionPrivate::initializeOpenCL()
{
    /* Identify a platform */
    cl_platform_id platform;
    int err = clGetPlatformIDs(1, &platform, NULL);
    if (err < 0)
    {
       throw runtime_error("Couldn't identify a platform");
    }

    /* Access a device */
    cl_device_id dev;
    cout << "retrieve GPU device" << endl;
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
    if (err == CL_DEVICE_NOT_FOUND)
    {
        cout << "fallback on CPU device" << endl;
        err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
    }

    if (err < 0)
    {
        throw runtime_error("Couldn't access any devices");
    }

    device = dev;

    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    if (err < 0)
    {
        throw runtime_error("Couldn't create a context");
    }

    buildProgram(RESOURCES_DIR "/convolution.cl");

    /* Create a kernel */
    kernel = clCreateKernel(program, "convolution", &err);
    if (err < 0)
    {
        throw runtime_error("Couldn't create a kernel");
    }

    /* Create a command queue */
    queue = clCreateCommandQueueWithProperties(context, device, 0, &err);
    if (err < 0)
    {
        throw runtime_error("Couldn't create a command queue");
    }
}

GpuConvolution::GpuConvolution(const Eigen::MatrixXf &filter, float factor)
    : Convolution(filter, factor), _private(new GpuConvolutionPrivate)
{
    _private->initializeOpenCL();
}

GpuConvolution::~GpuConvolution()
{
    delete _private;
}

void GpuConvolution::process(const Image &input, Image &output) const
{
    cl_int status = 0;

    const size_t width = input.width();
    const size_t height = input.height();
    const size_t imageSize = width * height * sizeof(unsigned char) * 4;

    output.reset(width, height);
    unsigned char *data = output.data();

    cl_image_desc desc;
    desc.image_type = CL_MEM_OBJECT_IMAGE2D;
    desc.image_width = input.width();
    desc.image_height = input.height();
    desc.image_depth = 0;
    desc.image_array_size = 0;
    desc.image_row_pitch = 0;
    desc.image_slice_pitch = 0;
    desc.num_mip_levels = 0;
    desc.num_samples = 0;
    desc.buffer = NULL;

    cl_image_format format;
    format.image_channel_order = CL_RGBA;
    format.image_channel_data_type = CL_UNSIGNED_INT8;

    // Create memory buffers on the device for each vector
    cl_mem mem_input = clCreateImage(_private->context, CL_MEM_READ_ONLY,
            &format, &desc, NULL, &status);
    ::clCheck(status);
    cl_mem mem_output = clCreateImage(_private->context, CL_MEM_WRITE_ONLY,
            &format, &desc, NULL, &status);
    ::clCheck(status);
    cl_mem mem_filter = clCreateBuffer(_private->context, CL_MEM_WRITE_ONLY,
            _filter.size() * sizeof(float), NULL, &status);
    ::clCheck(status);

    // Copy data to their respective memory buffers
    size_t origin[3] = {0, 0, 0};
    size_t region[3] = {width, height, 1};
    status = clEnqueueWriteImage(_private->queue, mem_input, CL_TRUE,
            origin, region, 0, 0,
            input.data(), 0, NULL, NULL);
    ::clCheck(status);
    status = clEnqueueWriteBuffer(_private->queue, mem_filter, CL_TRUE, 0,
            _filter.size() * sizeof(float), _filter.data(), 0, NULL, NULL);
    ::clCheck(status);

    // Create a sampler
    cl_sampler_properties properties[] = {
        CL_SAMPLER_NORMALIZED_COORDS, CL_FALSE,
        CL_SAMPLER_ADDRESSING_MODE, CL_ADDRESS_CLAMP_TO_EDGE,
        CL_SAMPLER_FILTER_MODE, CL_FILTER_NEAREST,
    0 };
    cl_sampler sampler = clCreateSamplerWithProperties(_private->context, properties, &status);
    ::clCheck(status);

    // Set the arguments of the kernel
    status = clSetKernelArg(_private->kernel, 0, sizeof(cl_mem), (void *)&mem_input);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 1, sizeof(cl_mem), (void *)&mem_output);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 2, sizeof(int), (void *)&width);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 3, sizeof(int), (void *)&height);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 4, sizeof(cl_mem), (void *)&mem_filter);
    ::clCheck(status);
    int fcols = _filter.cols();
    status = clSetKernelArg(_private->kernel, 5, sizeof(int), (void *)&fcols);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 6, sizeof(float), (void *)&_factor);
    ::clCheck(status);
    status = clSetKernelArg(_private->kernel, 7, sizeof(cl_sampler), (void *)&sampler);
    ::clCheck(status);

    // Execute the OpenCL kernel on the list
    size_t global_item_size[2] = {width, height}; // Process the entire lists
    size_t local_item_size[2] = {10, 10}; // Divide work items into groups
    status = clEnqueueNDRangeKernel(_private->queue, _private->kernel, 2, NULL,
            global_item_size, local_item_size, 0, NULL, NULL);
    ::clCheck(status);

    // Read the memory buffer on the device to the local variable
    status = clEnqueueReadImage(_private->queue, mem_output, CL_TRUE,
            origin, region, 0, 0,
            data, 0, NULL, NULL);
    ::clCheck(status);

    // Wait for the end
    status = clFlush(_private->queue);
    status = clFinish(_private->queue);

    // Clean up
    status = clReleaseMemObject(mem_output);
    status = clReleaseMemObject(mem_input);
    status = clReleaseMemObject(mem_filter);
}

// add_vectors.cl
// void GpuConvolution::process(const Image &input, Image &output) const
// {
//     int status = 0;

//     const size_t size = 1024;
//     vector<int> v1(size);
//     vector<int> v2(size);
//     for (int i = 0; i < size; ++i)
//     {
//         v1[i] = i;
//         v2[i] = size - i;
//     }

//     // Create memory buffers on the device for each vector
//     cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
//             size * sizeof(int), NULL, &status);
//     cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY,
//             size * sizeof(int), NULL, &status);
//     cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
//             size * sizeof(int), NULL, &status);

//     // Copy the lists A and B to their respective memory buffers
//     status = clEnqueueWriteBuffer(queue, a_mem_obj, CL_TRUE, 0,
//             size * sizeof(int), v1.data(), 0, NULL, NULL);
//     status = clEnqueueWriteBuffer(queue, b_mem_obj, CL_TRUE, 0,
//             size * sizeof(int), v2.data(), 0, NULL, NULL);

//     // Set the arguments of the kernel
//     status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);
//     status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&b_mem_obj);
//     status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&c_mem_obj);

//     // Execute the OpenCL kernel on the list
//     size_t global_item_size = size; // Process the entire lists
//     size_t local_item_size = 64; // Divide work items into groups of 64
//     status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
//             &global_item_size, &local_item_size, 0, NULL, NULL);

//     // Read the memory buffer C on the device to the local variable C
//     vector<int> result(size);
//     status = clEnqueueReadBuffer(queue, c_mem_obj, CL_TRUE, 0,
//             size * sizeof(int), result.data(), 0, NULL, NULL);

//     // Wait for the end
//     status = clFlush(queue);
//     status = clFinish(queue);

//     // Display the result to the screen
//     for (int i = 0; i < size; ++i) {
//         printf("%d + %d = %d\n", v1[i], v2[i], result[i]);
//     }

//     // Clean up
//     status = clReleaseMemObject(a_mem_obj);
//     status = clReleaseMemObject(b_mem_obj);
//     status = clReleaseMemObject(c_mem_obj);
// }

// histogram.cl
// void GpuConvolution::process(const Image &input, Image &output) const
// {
//     int status = 0;

//     const int imageElements = input.width() * input.height();
//     const size_t imageSize = imageElements * sizeof(unsigned char) * 4;
//     unsigned char* data = (unsigned char*)input.data();

//     cout << "elements: " << imageElements << endl;
//     cout << "size: " << imageSize << endl;

//     const size_t histLen = 256;
//     unsigned int* bufferHist = new unsigned int[histLen];
//     const size_t histSize = histLen * sizeof(unsigned int);

//     // Create memory buffers on the device for each vector
//     cl_mem mem_image = clCreateBuffer(context, CL_MEM_READ_ONLY,
//             imageSize, NULL, &status);
//     ::clCheck(status);
//     cl_mem mem_hist = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
//             histSize, NULL, &status);
//     ::clCheck(status);

//     // Copy the lists A and B to their respective memory buffers
//     status = clEnqueueWriteBuffer(queue, mem_image, CL_TRUE, 0,
//             imageSize, data, 0, NULL, NULL);
//     ::clCheck(status);
//     int zero = 0;
//     status = clEnqueueFillBuffer(queue, mem_hist, &zero,
//             sizeof(int), 0, histSize, 0, NULL, NULL);
//     ::clCheck(status);

//     // Set the arguments of the kernel
//     status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&mem_image);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 1, sizeof(int), (void *)&imageElements);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&mem_hist);
//     ::clCheck(status);

//     // Execute the OpenCL kernel on the list
//     size_t global_item_size = 1024; // Process the entire lists
//     size_t local_item_size = 64; // Divide work items into groups of 64
//     status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL,
//             &global_item_size, &local_item_size, 0, NULL, NULL);
//     ::clCheck(status);

//     // Read the memory buffer C on the device to the local variable C
//     status = clEnqueueReadBuffer(queue, mem_hist, CL_TRUE, 0,
//             histSize, bufferHist, 0, NULL, NULL);
//     ::clCheck(status);

//     // Wait for the end
//     status = clFlush(queue);
//     status = clFinish(queue);

//     // Display the result to the screen
//     for (int i = 0; i < histLen; ++i) {
//         printf("%d\n", bufferHist[i]);
//     }

//     // Clean up
//     status = clReleaseMemObject(mem_hist);
//     status = clReleaseMemObject(mem_image);

//     delete[] bufferHist;
// }

// rotate.cl
// void GpuConvolution::process(const Image &input, Image &output) const
// {
//     cl_int status = 0;

//     const float theta = 45.f;

//     const size_t width = input.width();
//     const size_t height = input.height();
//     const int imageElements = width * height;
//     const size_t imageSize = imageElements * sizeof(unsigned char) * 4;

//     cl_image_desc desc;
//     desc.image_type = CL_MEM_OBJECT_IMAGE2D;
//     desc.image_width = input.width();
//     desc.image_height = input.height();
//     desc.image_depth = 0;
//     desc.image_array_size = 0;
//     desc.image_row_pitch = 0;
//     desc.image_slice_pitch = 0;
//     desc.num_mip_levels = 0;
//     desc.num_samples = 0;
//     desc.buffer = NULL;

//     cl_image_format format;
//     format.image_channel_order = CL_RGBA;
//     format.image_channel_data_type = CL_UNSIGNED_INT8;

//     // Create memory buffers on the device for each vector
//     cl_mem mem_input = clCreateImage(context, CL_MEM_READ_ONLY,
//             &format, &desc, NULL, &status);
//     ::clCheck(status);
//     cl_mem mem_output = clCreateImage(context, CL_MEM_WRITE_ONLY,
//             &format, &desc, NULL, &status);
//     ::clCheck(status);

//     // Copy the lists A and B to their respective memory buffers
//     size_t origin[3] = {0, 0, 0};
//     size_t region[3] = {width, height, 1};
//     status = clEnqueueWriteImage(queue, mem_input, CL_TRUE,
//             origin, region, 0, 0,
//             input.data(), 0, NULL, NULL);
//     ::clCheck(status);

//     // Set the arguments of the kernel
//     status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&mem_input);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&mem_output);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 2, sizeof(int), (void *)&width);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 3, sizeof(int), (void *)&height);
//     ::clCheck(status);
//     status = clSetKernelArg(kernel, 4, sizeof(float), (void *)&theta);
//     ::clCheck(status);

//     // Execute the OpenCL kernel on the list
//     size_t global_item_size[2] = {width, height}; // Process the entire lists
//     size_t local_item_size[2] = {10, 10}; // Divide work items into groups of 64
//     status = clEnqueueNDRangeKernel(queue, kernel, 2, NULL,
//             global_item_size, local_item_size, 0, NULL, NULL);
//     ::clCheck(status);

//     // Read the memory buffer C on the device to the local variable C
//     unsigned char *data = new unsigned char[width * height * 4];
//     status = clEnqueueReadImage(queue, mem_output, CL_TRUE,
//             origin, region, 0, 0,
//             data, 0, NULL, NULL);
//     ::clCheck(status);
//     output.setData(data, Eigen::Vector2f(width, height));

//     // Wait for the end
//     status = clFlush(queue);
//     status = clFinish(queue);

//     // Clean up
//     status = clReleaseMemObject(mem_output);
//     status = clReleaseMemObject(mem_input);
// }

const char *getClErrorString(cl_int error)
{
switch (error) {
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}

void clCheck(int status)
{
    if (status != CL_SUCCESS)
    {
        cerr << "[ERROR] " << getClErrorString(status) << endl;
    }
}