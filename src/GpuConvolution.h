#ifndef __GPUCONVOLUTION_H__
#define __GPUCONVOLUTION_H__

#include "Convolution.h"

class GpuConvolution : public Convolution
{
public:
    GpuConvolution(const Eigen::MatrixXf &filter, float factor);
    ~GpuConvolution();

    void process(const Image &input, Image &output) const;

private:
	struct GpuConvolutionPrivate;
	GpuConvolutionPrivate *_private;
};

#endif /*__GPUCONVOLUTION_H__*/
